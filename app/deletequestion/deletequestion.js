'use strict';

angular.module('myApp.deletequestion', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/deletequestion', {
            templateUrl: 'deletequestion/deletequestion.html',
            controller: 'deletequestionCtrl'
        });
    }])

    .controller('deletequestionCtrl', ['$http','$scope',function($http ,$scope) {

    }]);