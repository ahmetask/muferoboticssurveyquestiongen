'use strict';

angular.module('myApp.showresult', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/showresult', {
            templateUrl: 'showresult/showresult.html',
            controller: 'showresultCtrl'
        });
    }])

    .controller('showresultCtrl', ['$http','$scope',function($http ,$scope) {
        $scope.q_id = 0 ;
        $scope.c_id = 0 ;
        $scope.result = [];
        $scope.getResult = function(q_id , c_id) {
            if(c_id==null)
                c_id=0;
            $scope.q_id = q_id ;
            $scope.c_id = c_id ;
            $http({
                method: 'GET',
                url: "http://api.muferobotics.org/v1/surveyresult/!muroSurveyP!17aa/"+$scope.q_id+"/"+$scope.c_id
            }).then(function successCallback(response) {
                var resarr = [];
                var r = response.data.content.a;
                if(r.length == 0)
                    $scope.result = [];
                    for(var  d  in r){
                        var res ;
                        var data = r[d];
                        if(c_id ==0 ){
                             res = {"question_id" : data.question_id,
                                       "question_text":data.question_text ,
                                        "question_text_en": data.question_text_en ,
                                        "answer_text": data.answer_text,
                                        "choice_text" : "",
                                        "choice_text_en": "",
                                        "choice_id" : 0,
                            }
                            resarr.push(res);
                        }
                        else {

                            res = {"question_id" : data.question_id ,
                                "question_text":data.question_text ,
                                "question_text_en": data.question_text_en ,
                                "anwer_text": "",
                                "choice_text" : data.choice_text,
                                "choice_text_en": data.choice_text_en,
                                "choice_id" : c_id,



                            }
                            resarr.push(res);
                        }

                        $scope.result = resarr ;
                    }


            }, function errorCallback(response) {
                console.log(response)
            });


        };

    }]);