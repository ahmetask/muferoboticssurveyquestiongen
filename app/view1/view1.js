'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$http','$scope',function($http ,$scope) {
    $http({
        method: 'GET',
        url: 'http://api.muferobotics.org/v1/questions'
    }).then(function successCallback(response) {
        $scope.questions = response.data.content.a ;
    }, function errorCallback(response) {
        console.log(response)
    });
}]);