'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$http','$scope',function($http ,$scope) {
    var choices = [] ;
    $scope.choiceButtonClass = "ui disabled button";
    $scope.choiceInputClass = "ui disabled button";
    $scope.Message = "Not Sended Yet";
    $http({
        method: 'GET',
        url: 'http://api.muferobotics.org/v1/questions'
    }).then(function successCallback(response) {
        $scope.survey_id = response.data.content.survey_id ;
    }, function errorCallback(response) {
        console.log(response)
    });
    $scope.qtypes = [
        {type : "Multi line", value : 1},
        {type : "CheckBox", value : 4},
        {type : "Radio", value : 0},
        {type : "Number", value : 2}

    ];
    $scope.changeQtype = function(item_selected) {
        $scope.choiceButtonClass = "ui disabled button";
        $scope.choiceInputClass = "ui disabled button";
        $scope.qtype = item_selected.value;
        if($scope.qtype == 0 || $scope.qtype == 4){
            $scope.choiceButtonClass = "ui  button";
            $scope.choiceInputClass = "ui  button";
        }
    };
    $scope.addChoice = function(choiceEng , choiceTr) {

        var choice = {"choice_text": choiceTr, "choice_text_en" : choiceEng}
        choices.push(choice);
        $scope.choices =choices;

    };
    $scope.removeLastChoice = function() {


        choices.pop();
        $scope.choices =choices;

    };
    $scope.postQuestion = function (question_text, question_text_en) {
        var data = { 'pass' : "!muroSurveyP!17aa",
            'survey_id' : $scope.survey_id,
            'question_text': question_text,
            'question_text_en': question_text_en,
            'choices' : $scope.choices ,
            'type': $scope.qtype
        }

        $http.post('http://api.muferobotics.org/v1/questions/new', JSON.stringify(data)).then(function (response) {
            $scope.Message = "Sended";
        }, function (response) {

            console.log(response);

        });


    };


}]);